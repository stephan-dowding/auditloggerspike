﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace AuditLogger
{
    public interface IInsertStuff
    {
        void DoInsert();
        SqlTransaction Transaction { set; }
        Guid PrimaryKey { get; }
        int Number { get; set; }
        string Text { get; set; }
        DateTime Date { get; set; }
    }

    public class InsertStuff : IInsertStuff
    {
        private const string Sql = @"
INSERT INTO SomeTable
(
    PrimaryKey,
    Number,
    Text,
    Date
)
VALUES
(
    @PrimaryKey,
    @Number,
    @Text,
    @Date
)
";

        public InsertStuff()
        {
            PrimaryKey = Guid.NewGuid();
        }

        [PrimaryKey]
        public Guid PrimaryKey { get; private set; }
        
        [Data]
        public int Number { get; set; }

        [Data]
        public string Text { get; set; }
        
        [Data]
        public DateTime Date { get; set; }

        public SqlTransaction Transaction { private get; set; }

        public void DoInsert()
        {
            
            var command = new SqlCommand(Sql);
            command.Connection = Transaction.Connection;
            command.Transaction = Transaction;
            command.Parameters.AddWithValue("@PrimaryKey", PrimaryKey);
            command.Parameters.AddWithValue("@Number", Number);
            command.Parameters.AddWithValue("@Text", Text);
            command.Parameters.AddWithValue("@Date", Date);

            command.ExecuteNonQuery();
        }
    }
}