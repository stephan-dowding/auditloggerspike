﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AuditLogger
{
    public partial class InsertDataForm : Form
    {
        private readonly IInsertStuff _stuffInserter;

        public InsertDataForm(IInsertStuff stuffInserter)
        {
            _stuffInserter = stuffInserter;
            InitializeComponent();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            _stuffInserter.Text = TextBox.Text;
            _stuffInserter.Number = TrackBar.Value;
            _stuffInserter.Date = DateTimePicker.Value;
            _stuffInserter.DoInsert();
        }
    }
}
