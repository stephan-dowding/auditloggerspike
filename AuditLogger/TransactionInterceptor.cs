﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace AuditLogger
{
    public class TransactionInterceptor : IInterceptionBehavior
    {
        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            if (input.MethodBase.Name == "DoInsert")
            {
                var target = (IInsertStuff)input.Target;

                var connection = new SqlConnection
                    {
                        ConnectionString = ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString
                    };

                connection.Open();

                var transaction = connection.BeginTransaction();
                target.Transaction = transaction;
                input.InvocationContext.Add("Transaction", transaction);
                try
                {
                    return getNext()(input, getNext);
                }
                finally
                {
                    transaction.Commit();
                    connection.Close();
                }

            }
            return getNext()(input, getNext);
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public bool WillExecute { get { return true; } }
    }
}