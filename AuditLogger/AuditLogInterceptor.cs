﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace AuditLogger
{
    public class AuditLogInterceptor : IInterceptionBehavior
    {
        private const string AuditSql = @"
    INSERT INTO [dbo].[Audit]
           ([PrimaryKey]
           ,[UserName]
           ,[EventTime]
           ,[TableName]
           ,[RowKey]
           ,[ChangeType]
           ,[Data])
     VALUES
           (@PrimaryKey
           ,@UserName
           ,@EventTime
           ,@TableName
           ,@RowKey
           ,@ChangeType
           ,@Data)";

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            if (input.MethodBase.Name == "DoInsert")
            {
                var target = input.Target;
                var targetType = target.GetType();

                var rowKey =
                    (Guid)
                    targetType.GetProperties()
                              .First(prop => Attribute.IsDefined(prop, typeof (PrimaryKeyAttribute)))
                              .GetValue(target);
                var dataProps =
                    targetType.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof (DataAttribute)));

                var xml = "<data>";
                xml = dataProps.Aggregate(xml,
                                    (xmlAcc, prop) =>
                                    xmlAcc +
                                    string.Format("<item column=\"{0}\">{1}</item>", prop.Name, prop.GetValue(target)));
                xml += "</data>";

                var command = new SqlCommand(AuditSql);
                var transaction = (SqlTransaction) input.InvocationContext["Transaction"];
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.AddWithValue("@PrimaryKey", Guid.NewGuid());
                command.Parameters.AddWithValue("@UserName", "InjectMe");
                command.Parameters.AddWithValue("@TableName", "GetFromAttribute");
                command.Parameters.AddWithValue("@ChangeType", "INSERT?");
                command.Parameters.AddWithValue("@EventTime", DateTime.UtcNow);
                command.Parameters.AddWithValue("@RowKey", rowKey);
                command.Parameters.AddWithValue("@Data", xml);

                command.ExecuteNonQuery();
            }
            return getNext()(input, getNext);
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public bool WillExecute {
            get { return true; }
        }
    }



    public class DataAttribute : Attribute
    {
    }

    public class PrimaryKeyAttribute : Attribute
    {
    }
}