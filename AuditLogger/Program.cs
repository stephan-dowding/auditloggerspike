﻿using System;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace AuditLogger
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            var container = new UnityContainer();
            container.AddNewExtension<Interception>();
            container.RegisterType<IInsertStuff, InsertStuff>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<TransactionInterceptor>(),
                new InterceptionBehavior<AuditLogInterceptor>()
                );


            Application.Run(container.Resolve<InsertDataForm>());
        }
    }
}
