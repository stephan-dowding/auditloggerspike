﻿CREATE TABLE dbo.SomeTable
	(
	PrimaryKey uniqueidentifier NOT NULL,
	Number int NULL,
	Text nvarchar(50) NULL,
	Date datetime NULL
	)
GO
ALTER TABLE dbo.SomeTable ADD CONSTRAINT
	PK_SomeTable PRIMARY KEY CLUSTERED 
	(
	PrimaryKey
	)
GO


CREATE TABLE dbo.Audit
	(
	PrimaryKey uniqueidentifier NOT NULL,
	UserName nvarchar(50) NOT NULL,
	EventTime datetime NOT NULL,
	TableName nvarchar(50) NOT NULL,
	RowKey uniqueidentifier NOT NULL,
	ChangeType nvarchar(10) NOT NULL,
	Data xml NULL
	)
GO
ALTER TABLE dbo.Audit ADD CONSTRAINT
	PK_Audit PRIMARY KEY CLUSTERED 
	(
	PrimaryKey
	)
GO